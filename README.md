<!--
This file is part of stock-ticker,
a simple Binance stock-ticker app for the command line:
https://gitlab.com/lemberger/stock-ticker

SPDX-FileCopyrightText: 2020 Thomas Lemberger <https://thomaslemberger.com>

SPDX-License-Identifier: Apache-2.0
-->

# Stock-Ticker for Crypto Coins

[![Apache 2.0 License](https://img.shields.io/badge/license-Apache--2-brightgreen.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)

A minimalistic stock ticker for crypto coin-pairs, written in Rust.
A single execution returns the current price for the specified crypto-coin pairs.
The price is queried from the [Binance API](https://binance-docs.github.io/apidocs/).


## Building

To build stock-ticker, run `cargo build` in the root directory of this project.

## Usage

To execute stock-ticker, use `cargo run`, or find the `stock-ticker` executable
in the `target` directory after building.
By default, it is `target/debug/stock-ticker`.

Run `stock-ticker --help` (or `cargo run -- --help`) for usage information.

By default, only the price for BTC in USDT is queried.
To query other coin pairs, specify a config file ([example][1]) on the command-line, e.g.:

```shell
stock-ticker --config examples/config.json
```

[1]: examples/config.json

## License

All components of this project are licensed under Apache-2.0, copyright by Thomas Lemberger, 2020.
