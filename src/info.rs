// This file is part of stock-ticker,
// a simple Binance stock-ticker app for the command line:
// https://gitlab.com/lemberger/stock-ticker
//
// SPDX-FileCopyrightText: 2020 Thomas Lemberger <https://thomaslemberger.com>
//
// SPDX-License-Identifier: Apache-2.0

pub mod binance;
pub mod error;
