// This file is part of stock-ticker,
// a simple Binance stock-ticker app for the command line:
// https://gitlab.com/lemberger/stock-ticker
//
// SPDX-FileCopyrightText: 2020 Thomas Lemberger <https://thomaslemberger.com>
//
// SPDX-License-Identifier: Apache-2.0

use serde::Deserialize;

#[derive(Deserialize)]
struct TickerResponse {
    symbol: String,
    markPrice: String,
    lastFundingRate: String,
    nextFundingTime: i64,
    time: i64,
}

const INFO_ENDPOINT: &str = "https://testnet.binancefuture.com/fapi/v1/premiumIndex";

fn get_info(symbol: &String) -> Result<TickerResponse, reqwest::Error> {
    let endpoint = format!("{}?symbol={}", INFO_ENDPOINT, symbol);
    reqwest::blocking::get(&endpoint)?.json::<TickerResponse>()
}

pub fn get_price(symbol: &String) -> Result<f64, crate::info::error::Error> {
    let info = get_info(symbol)?;
    let val = info.markPrice.parse::<f64>()?;
    Ok(val)
}
