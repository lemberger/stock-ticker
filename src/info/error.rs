// This file is part of stock-ticker,
// a simple Binance stock-ticker app for the command line:
// https://gitlab.com/lemberger/stock-ticker
//
// SPDX-FileCopyrightText: 2020 Thomas Lemberger <https://thomaslemberger.com>
//
// SPDX-License-Identifier: Apache-2.0

#[derive(Debug)]
pub enum Error {
    Query(reqwest::Error),
    Parse(std::num::ParseFloatError),
}

impl From<reqwest::Error> for Error {
    fn from(error: reqwest::Error) -> Self {
        Error::Query(error)
    }
}

impl From<std::num::ParseFloatError> for Error {
    fn from(error: std::num::ParseFloatError) -> Self {
        Error::Parse(error)
    }
}
