// This file is part of stock-ticker,
// a simple Binance stock-ticker app for the command line:
// https://gitlab.com/lemberger/stock-ticker
//
// SPDX-FileCopyrightText: 2020 Thomas Lemberger <https://thomaslemberger.com>
//
// SPDX-License-Identifier: Apache-2.0

mod info;
use info::error::Error;
use serde::Deserialize;
use std::collections::BTreeMap;
use std::fs;
use std::path::PathBuf;
use std::thread;

const DEFAULT_CONFIG: &str = ".config/stock-ticker/config.json";

#[derive(Deserialize)]
struct Config {
    symbols: Vec<String>,
}

impl Config {
    fn new(path: &PathBuf) -> Result<Self, Box<dyn std::error::Error>> {
        match fs::read_to_string(path) {
            Ok(c) => Ok(serde_json::from_str(&c)?),
            Err(e) => {
                eprintln!("Reading config file {:?} failed", path);
                Err(Box::new(e))
            }
        }
    }

    fn default() -> Result<Self, Box<dyn std::error::Error>> {
        Ok(Config {
            symbols: vec!["BTCUSDT".to_string()],
        })
    }
}

fn get_global_config_path() -> Option<PathBuf> {
    if let Some(mut dir) = dirs::home_dir() {
        dir.push(DEFAULT_CONFIG);
        if dir.is_file() {
            return Some(dir);
        }
    }
    None
}

pub fn run(config_file: Option<PathBuf>) -> Result<(), Box<dyn std::error::Error>> {
    let config = match config_file {
        Some(p) => Config::new(&p),
        None => match get_global_config_path() {
            Some(global_config) => Config::new(&global_config),
            None => Config::default(),
        },
    }?;

    for (s, p) in get_price(config.symbols) {
        println!("{}: {}", s, p);
    }
    Ok(())
}

// BTreeMap sorts entries by key for us. This allows deterministic output.
fn get_price(symbols: Vec<String>) -> BTreeMap<String, f64> {
    let mut prices = BTreeMap::new();
    let mut workers = vec![];
    for s in symbols {
        workers.push((
            s.clone(),
            thread::spawn(move || -> Result<f64, Error> { info::binance::get_price(&s) }),
        ));
    }
    for (s, w) in workers {
        let mark_price = match w.join().unwrap() {
            Ok(p) => p,
            Err(e) => {
                eprintln!("Failed to get ticker price: {:?}", e);
                continue;
            }
        };
        prices.insert(s, mark_price);
    }
    prices
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn config_default() {
        Config::default().unwrap();
    }

    #[test]
    fn config_from_file() {
        let config_json = PathBuf::from("examples/config.json");
        Config::new(&config_json).unwrap();
    }

    #[test]
    fn config_fails_from_missing_file() {
        let missing_file = PathBuf::from("/tmp/stock-ticker/filethatdoesntexist");
        match Config::new(&missing_file) {
            Ok(_) => panic!(
                "Config created from file that shouldn't exist: {:?}",
                missing_file
            ),
            Err(_) => (), // expected
        };
    }

    #[test]
    fn get_price_bitcoin() {
        // This test requires a network connection
        let symbol = "BTCUSDT";

        let result = get_price(vec![symbol.to_string()]);

        assert_eq!(1, result.len());
        assert!(result.contains_key(symbol));
        match result.get(symbol) {
            Some(price) => assert!(price > &0.0),
            None => panic!(),
        };
    }
}
