// This file is part of stock-ticker,
// a simple Binance stock-ticker app for the command line:
// https://gitlab.com/lemberger/stock-ticker
//
// SPDX-FileCopyrightText: 2020 Thomas Lemberger <https://thomaslemberger.com>
//
// SPDX-License-Identifier: Apache-2.0

use std::path::PathBuf;
use std::process;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Cli {
    #[structopt(short, long, parse(from_os_str))]
    config: Option<PathBuf>,
}

fn main() {
    let args = Cli::from_args();
    if let Err(e) = stock_ticker::run(args.config) {
        eprintln!("Application error: {}", e);
        process::exit(1);
    }
}
